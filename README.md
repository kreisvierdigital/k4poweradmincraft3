# k4 PowerAdmin plugin for Craft CMS 3.x

Tool to hide and show fields in CP regarding Admin Permissions.

![Screenshot](resources/img/plugin-logo.png)

## Installation

To install k4 PowerAdmin, follow these steps:

1. install with Composer via `composer require k4/k4poweradmin`
4. Activate/Install the plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `k4poweradmin` for Craft to see it. 

k4 PowerAdmin works with Craft 3.x.

## k4 PowerAdmin Overview

Tool to hide and show fields in CP regarding Admin Permissions.


## Using k4 PowerAdmin

Every Field Name with "admin-yourfieldname" will automatically hide for none admin users.


Brought to you by [Thomas Bauer, Pascal Ujak](http://www.kreisvier.ch)
