<?php
/**
 * k4 PowerAdmin plugin for Craft CMS 3.x
 *
 * A simple tool to hide and show fields in CP based on administrative permissions
 *
 * @link      http://www.kreisvier.ch
 * @copyright Copyright (c) 2017-2020 Thomas Bauer, Pascal Ujak, Mischa Sprecher
 */

namespace k4\k4poweradmin;

use k4\k4poweradmin\assetbundles\K4PoweradminAsset;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\View;
use craft\events\RegisterUserPermissionsEvent;
use craft\services\UserPermissions;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;
use yii\base\Event;


/**
 * Class K4Poweradmin
 *
 * @author    Thomas Bauer, Pascal Ujak, Mischa Sprecher
 * @package   K4Poweradmin
 * @since     2.1.2
 */
class K4Poweradmin extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var K4encodeemails
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '2.1.2';

    // Public Methods
    // =========================================================================

    public function init()
    {
        parent::init();
        self::$plugin = $this;

        // Only check on CP requests
        if (!Craft::$app->getRequest()->isCpRequest) {
            return;
        }

        // Do something after we're installed
        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                    Craft::$app->session->setNotice(Craft::t('k4-poweradmin','install-success'));
                }
            }
        );

        Event::on(View::class, View::EVENT_END_BODY, function(Event $event) {

            Craft::$app->getView()->registerAssetBundle(K4PoweradminAsset::class);

            $bodyAdmin = '';
            $additionalCSS = 'body.notAdmin div[id^="fields-admin"]{display:none;}';

            $user = Craft::$app->user->getIsGuest();

            // Check if User is Admin
            if ($user == false ) {

                if (Craft::$app->user->identity->admin) {
                    $bodyAdmin = '$("body").addClass("isAdmin");';
                }

                else {
                    $bodyAdmin = '$("body").addClass("notAdmin");';
                }

            }

            $userSession = Craft::$app->user->checkPermission('k4PowerAdminSettings');

            //if User has not k4poweradminSettings privileges
            if ($userSession == false) {
                $additionalCSS = $additionalCSS . ' body.notAdmin .re-html{display:none;}';
            }


            Craft::$app->view->registerJs($bodyAdmin);
            Craft::$app->view->registerCSS($additionalCSS);

        });


        Event::on(UserPermissions::class, UserPermissions::EVENT_REGISTER_PERMISSIONS, function(RegisterUserPermissionsEvent $event) {
            $event->permissions['k4 PowerAdmin'] = [
                'k4PowerAdminSettings' => ['label' => 'Show HTML in WYSIWYG Editor']
            ];
        });


        Craft::info(Craft::t('k4-poweradmin', '{name} plugin-loaded', ['name' => $this->name]), __METHOD__);

    }

}
