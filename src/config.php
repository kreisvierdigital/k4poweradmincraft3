<?php
/**
 * k4 PowerAdmin plugin for Craft CMS 3.x
 *
 * Tool to hide and show fields in CP regarding Admin Permissions
 *
 * @link      http://www.kreisvier.ch
 * @copyright Copyright (c) 2017-2020 Thomas Bauer, Pascal Ujak, Mischa Sprecher
 */


return [

    // This controls blah blah blah
    "someSetting" => true,

];
