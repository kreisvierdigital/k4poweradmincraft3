<?php

namespace k4\k4poweradmin\assetbundles;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Thomas Bauer, Pascal Ujak, Mischa Sprecher
 * @package   K4Poweradmin
 * @since     2.1.2
 */

class K4PoweradminAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = __DIR__.'/dist';


        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'admin.js',
        ];

        parent::init();
    }
}
