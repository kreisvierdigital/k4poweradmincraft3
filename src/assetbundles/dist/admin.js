$(document).ready(function() {

    $("body.notAdmin div[id^='fields-admin'] .last .btngroup").remove();
    $("body.notAdmin div[id^='fields-admin']").hide();

    var entryTypeSelector = $("#entryType");
    if (entryTypeSelector.length) {
        if(entryTypeSelector.find(":selected").text().indexOf("admin")== 0){
            //hide other values 
            entryTypeSelector.find(":not(:selected)").hide();

        }else{
            //hide all admin values
            entryTypeSelector.find(":contains('admin')").hide();
        };
    };
    
});