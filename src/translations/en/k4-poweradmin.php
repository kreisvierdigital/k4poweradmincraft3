<?php
/**
 * k4 PowerAdmin plugin for Craft CMS 3.x
 *
 * Tool to hide and show fields in CP regarding Admin Permissions
 *
 * @link      http://www.kreisvier.ch
 * @copyright Copyright (c) 2017 Thomas Bauer, Pascal Ujak
 */

/**
 * k4 PowerAdmin en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t()`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Thomas Bauer, Pascal Ujak
 * @package   K4Poweradmin
 * @since     2.1.0
 */

return [
    'plugin-loaded'     => 'plugin loaded successfully',
    'install-success'   =>  'plugin installed successfully',];
