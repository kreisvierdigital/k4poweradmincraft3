# k4 PowerAdmin Changelog

All notable changes to this project will be documented in this file.


## 2.1.1 - 2020.12.14
### Fixed
- fixed psr-4 issues

## 2.1.0 - 2018.07.06
### Fixed
- namespace issues
- updated plugin handle to new format

## 2.0.0 - 2017.03.09
### Added
- Initial release
